// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

const f1 = require('./problem6.cjs')

const data = require('./cars.cjs')
car_array = data.inventory;
const num_cars = car_array.length


result= f1(car_array);
console.log(JSON.stringify(result));