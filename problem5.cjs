module.exports = problem_5;

let row;

function problem_5(car_array){
    let old_cars = [];
    for(row = 0; row < car_array.length;row++){
        if (car_array[row].car_year < 2000){
            old_cars.push(car_array[row]);
        }
    }
    console.log(old_cars); 
    return "Number of cars older than 2000 is " + old_cars.length + ".";
}

