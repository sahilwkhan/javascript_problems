// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.


const f1 = require('./problem5.cjs')

const data = require('./cars.cjs')
car_array = data.inventory;
const num_cars = car_array.length


result= f1(car_array);
console.log(result);