// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.


const f1 = require('./problem3.cjs')

const data = require('./cars.cjs')
car_array = data.inventory;
const num_cars = car_array.length


result = f1(car_array);
console.log(result);