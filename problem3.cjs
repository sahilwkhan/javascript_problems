module.exports = problem_3;


let main_iterator, second_iterator,temp;
function problem_3(car_array){
    for(main_iterator = 0; main_iterator < car_array.length-1;main_iterator++){

        for(second_iterator = main_iterator; second_iterator < car_array.length;second_iterator++){
            if (car_array[main_iterator].car_make  > car_array[second_iterator].car_make){
                temp = car_array[main_iterator];
                car_array[main_iterator] = car_array[second_iterator];
                car_array[second_iterator] = temp; 
            }
        }
    
    }
    return car_array
}

