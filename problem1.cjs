module.exports = problem_1;


let row ,car;

function problem_1(car_array,car_id){
    if (car_array == undefined){
        return [];
    }
    else if(!Array.isArray(car_array)){
        return [];
    }
    else if ( !Number.isInteger(car_id)){
        return [];
    }
    else{
        for( row = 0; row < car_array.length;row++){
            car = car_array[row]
            if (car['id'] == car_id){
                return car;  
            }
        }
    }
}
