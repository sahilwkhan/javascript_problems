// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of: 
const data = require('./cars.cjs')
car_array = data.inventory;
const num_cars = car_array.length

let output;

const p1 = require('./problem1.cjs')    

result = p1(car_array,[]);
console.log(result);


if (result.id != undefined && result.car_year != undefined && result.car_make != undefined && result.car_model != undefined){
    output  = "Car with id "+ result.id + " was made in " + result.car_year + " by ";
    output += result.car_make + " and it's model is " + result.car_model+ ".";
    console.log(output)
}else{
    undefined;
}


