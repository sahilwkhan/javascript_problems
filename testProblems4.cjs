// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.

const f1 = require('./problem4.cjs')

const data = require('./cars.cjs')
car_array = data.inventory;
const num_cars = car_array.length

result = f1(car_array);
console.log(result);